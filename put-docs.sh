#!/usr/bin/env bash

set -e

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

PROJECT_ID="$1" # lookup in repo settings
FILE="$2"
JOB="artifacts" # change if necessary
BRANCH="master"

echo "Downloading artifacts into temporary directory"
cd `mktemp -d`
curl \
	--header "JOB-TOKEN: $CI_JOB_TOKEN" \
	"https://git.dbogatov.org/api/v4/projects/$PROJECT_ID/jobs/artifacts/$BRANCH/download?job=$JOB" \
> artifacts.zip

echo "Extracting files"
unzip artifacts.zip

barefile="${FILE##*/}"

echo "Moving file"
mkdir -p $OLDPWD/dist/assets/custom/docs/
mv $FILE.pdf $OLDPWD/dist/assets/custom/docs/$barefile.pdf

echo "Done"
